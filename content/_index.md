# (l'ICT che vorrei nel...) La P.A. che vorrei {#mainrant}

Un grido di dolore che arriva dal profondo del cuore di qualcuno che vorrebbe una P.A. che utilizzasse al meglio le tecnologie ICT...

## Autori {#authors}

**Damiano Verzulli** \
Ultra cinquantenne, che da oltre 20 anni cerca di vivere di ICT (...in Abruzzo). \
mail: damiano@verzulli.it - mastodon: [@verzulli@fosstodon.org](https://fosstodon.org/@verzulli)

## TL/DR; {#foreword}

Ogni volta che vediamo una procedura web che non funziona come dovrebbe... che anziché semplificarci la vita, ce la complica... che addirittura, anziché farci guadagnare tempo, ce ne fa perdere (...ed a volte, TANTO)... che alcune volte, a causa di questi problemi, se ne generano altri, ben piu' importanti... ogni volta, in queste situazioni, il nostro cuore *sanguina*; il nostro cervello *non capisce*; il nostro livello di nervosismo *aumenta*.

*"Perché funziona cosi' male?"*, *"Perchè non è stata scritta come si dovrebbe?"*, *"Perché nessuno si accorge di questo problema?"* - Queste sono le domande ricorrenti che, puntualmente, ci poniano.

Noi vorremmo un ICT Pubblico che funzionasse bene; che semplificasse la vita ai cittadini; e che fosse "minimo", quasi "invisibile". Che funzionasse... e basta.

Ma dato che non l'abbiamo visto... e non ci pare di vederlo all'orizzonte... ci siamo decisi a mettere nero su bianco questa nostra **"lista dei desideri"**.

L'obiettivo è semplice: fornire spunti ai livelli decisionali, affinché, nell'amministrare "la Cosa Pubblica", possa pescare qui dentro, con la certezza di trovare un argomento serio, utile ed interessante (...per i cittadini)

## Lista disordinata di spunti {#introduction}

1. {{< label TASSE >}} Vorrei poter pagare un "F24 semplificato", sul sito dell'Agenzia delle Entrate, senza essere costretto a fare reverse-engineering delle procedure esistenti (...per scoprire che ~~NON è possibile~~ è possibile, ma è piu' complesso della procedura "standard");

2. {{< label SCUOLA >}} Vorrei che il Registro Elettronico mi inviasse notifiche real-time, quando gli insegnanti pubblicano "i compiti per casa", oppure la "sintesi dei lavori svolti". Non voglio essere costretto a visualizzare interfacce web di fornitori terzi, che hanno sviluppato tali interfacce senza avere figli in eta' scolare;

3. {{< label SCUOLA >}} Vorrei che la scuola **NON** utilizzasse Whatsapp per le sue comunicazioni (insegnante-insegnante e insegnante-alunno). Vorrei che venissero predisposte piattaforme **libere** e **aperte** (es.: istanze [matrix](https://matrix.org/) coordinate dagli USR o dalle singole scuole; e se proprio l'USR non se la sente, chiedesse supporto all'Ateneo più vicino);

4. {{< label SCUOLA >}} Vorrei che la scuola **NON** favorisse in alcun modo l'utilizzo di Facebook, Twitter o Youtube, ma che al loro posto adottasse e promuovesse piattaforme **libere** e **aperte** come [mastodon](https://joinmastodon.org/) e [Peertube](https://joinpeertube.org/);

4. {{< label SCUOLA >}} Vorrei che gli insegnanti non utilizzassero Classroom o Teams per pubblicare una riga di testo contente un link ad un video di Youtube, chiedendo ai corsisti di guardarlo. Vorrei che questo risultato venisse fatto inviando una semplice mail (o, al piu', un messaggio su un canale Matrix, o su un qualche strumento collaborativo "libero")

5. {{< label SCUOLA >}} {{< label UNIVERSITÀ >}} Vorrei che ogni Ateneo adottasse un certo numero di scuole e che offrisse loro le risorse per erogare i servizi di cui sopra;

6. {{< label SPID >}} Vorrei poter accedere a SPID senza essere costretto ad utilizzare i servizi di Google, ma semplicemente utilizzando tecnologie **libere** e **aperte** (come TOTP) per la generazione del secondo fattore di autenticazione;

7. {{< label TASSE >}} Vorrei che quando viene emessa una cartella esattoriale, un avviso di accertamente, una multa... o un qualsiasi atto che mi riguardi, io riceva near-real-time, una PEC con tutte le informazioni del caso;

8. {{< label SANITÀ >}} Vorrei che quando entro in Pronto Soccorso, anziché vedere un monitor da 42" che indica il numero di pazienti in attesa, divisi per codice, quel monitor mi indichi il tempo medio di attesa, per codice di colore, sulla base dei dati raccolti nell'ultimo mese o nell'analogo periodo dello scorso anno (stessa settimana; stesso giorno; stessa fascia oraria);

9. {{< label TRASPARENZA >}} Vorrei che sul sito web del mio Comune fosse disponibile, in evidenza, una pagina che riporti --aggiornata REAL-TIME-- il numero dei residenti; la composizione dei costi maturati dall'inizio dell'anno (torta); la composizione delle entrate previste in bilancio (torta); la quantita' di tasse comunali "emesse" e quelle effettivamente "riscosse"; l'incidenza dei costi del personale rispetto alla spesa complessiva;

10. {{< label SOSTENIBILITÀ >}} Vorrei che all'attenzione oggi riservata all'ENERGIA venisse affiancata una analoga attenzione per l'ACQUA. Vorrei che tutti i contatori fossero "elettronici" e che i relativi consumi fossero recuperati "near-real-time". Vorrei poter consultare una dashboard con le statistiche nazionali sulla distribuzione, sui consumi e sui trend del caso;

11. {{< label SVILUPPO_ICT >}} Vorrei poter sostituire il software che è in esecuzione su quella marea di dispositivi "smart" (smart-watch; smart-bracelet; smart-tv; vacuum-cleaner; videocamere di sorveglianza; etc.) che continuamente ci inonda. Vorrei scrollarmi di dosso l'ansia che tali dispositivi inviino i "miei" dati da quache parte, senza che io me ne accorga;

12. {{< label SVILUPPO_ICT >}} Vorrei poter continuare a frequentare il mio meccanico di fiducia e vorrei che la sua professione possa continuare ad esistere. Vorrei che lui potesse fare quello che ha sempre potuto fare, in epoca "meccanica", e che le limitazioni di cui soffre attualmente, a causa dell'utilizzo intensivo del software negli autoveicoli, si riducessero al minimo;

13. {{< label SVILUPPO_ICT >}} Vorrei che nei corsi universitari di Informatica, agli studenti venisse chiesto di prendere parte a qualche progetto esistente; Vorrei che gli Atenei supportassero direttamente almeno uno di tali progetti, contribuendone allo sviluppo; Vorrei che il mondo Univ prendesse in mano Apache James e lo utilizzasse per riacquisire il controllo della propria Posta Elettronica;

14. {{< label SVILUPPO_ICT >}} Vorrei che a coloro che ne hanno la competenza e la capacità venga data la possibilita' concreta di esprimersi nella gestione delle infrastrutture informatiche di cui hanno il controllo. Vorrei che queste persone non siano attanagliate dal dubbio costante di poter essere denunciate per violazione della privacy. Vorrei che tale ansia fosse trasferita ai produttori di software proprietario, o di soluzioni "cloud", che fanno della "obscurity" il proprio fiore nell'occhiello;

15. {{< label SVILUPPO_ICT >}} {{< label SCUOLA >}} Vorrei che alla fine delle scuole superiori, tutti gli studenti abbiano messo le mani almeno una volta su un ESP8266 --magari donatogli dalla scuola-- e ci siano connessi WiFi con il proprio cellulare; Vorrei che tutti avessero acceso almeno una volta il LED13 di un Arduino. Vorrei che avessero sperimentato i concetti di "consumo energetico", di "durata di una batteria", di "dispersione termica";

16. {{< label SVILUPPO_ICT >}} {{< label SCUOLA >}} Vorrei che nella piazza del centro commerciale a me piu' vicino, venisse organizzato almeno un "micromouse challenge", sponsorizzato dalle aziende che si occupano di automazione e promosso/sostenuto dalle scuole e dagli Atenei con corsi STEM;

17. {{< label SCUOLA >}} Vorrei che i libri di testo scolastici restassero "di carta", senza appendici digitali. E se è proprio necessario espanderli digitalmente, allora vorrei che la fruibilita' dei contenuti seguisse uno standard unico, e che non fossi costretto ad installare APP proprietarie, CODEC proprietari, o altre tecnologie decise dal singolo editore (e quindi incompatibili fra testi dello stesso anno scolastico... dello stesso studente). Vorrei che i contenuti digitali fossero banalmente fruibili, via web standard