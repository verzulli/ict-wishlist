# 2023 ICT PA wishlist

> This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

---

## 1. Cos'è questo progetto?

È una banale "lista dei desideri", completamente orientata all'utilizzo dell'ICT nella Pubblica Amministrazione.

È una lista che --nello spirito di chi la ha alimentata-- puo' essere utile a chi ha la responsabilità di indirizzare l'evoluzione del nostro Paese, oppure a chi ha "solo" il compito di gestirlo.

## 2. Cos'è questo progetto, tecnicamente?

È il repository dei file che generano i contenuti del sito, utilizzando il motore di [Hugo](https://gohugo.io/)

## 3. Posso proporre modifiche/integrazioni?

Certo che si! Apri una pull-request...

## 4. Come posso visualizzare il progetto, in locale?

Hai bisogno di Hugo, che quindi ti devi organizzare per recuperare ed installare localmente.

Poi:

1. `git clone` del progetto
2. `cd 2023_ICT_PA_wishlist`
3. `hugo server -b http://localhost --disableFastRender .`
4. punta il browser su: `http://localhost:1313/`


