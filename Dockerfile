# STEP 1: Istanzia un container Hugo e compila il sito web
FROM docker.io/klakegg/hugo:0.107.0-ext-asciidoctor as builder

WORKDIR /mnt/projectRoot
COPY . .
RUN hugo -s /mnt/projectRoot -d /mnt/projectRoot/public

# STEP 1: Istanzia un container Hugo e compila il sito web
FROM alpine:latest
RUN apk add thttpd
RUN adduser -D static
USER static
RUN mkdir -p /home/static/wishlist

WORKDIR /home/static/wishlist

COPY --from=builder /mnt/projectRoot/public .

EXPOSE 3000

CMD ["thttpd", "-D", "-h", "0.0.0.0", "-p", "3000", "-d", "/home/static", "-u", "static", "-l", "-", "-M", "60"]
